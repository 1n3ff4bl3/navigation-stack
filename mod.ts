export type ViewContext = Record<string, unknown>;

export interface View<C extends ViewContext> {
  name: string;
  render: (
    stack: NavigationStack<C>
  ) => Promise<HTMLElement | DocumentFragment> | HTMLElement | DocumentFragment;
}

export default class NavigationStack<C extends ViewContext> {
  public stack: View<C>[] = [];
  public container: HTMLElement;

  constructor(root: HTMLElement, initialView: View<C>, public context: C) {
    this.container = root;
    this.push(initialView);
  }

  public getCurrentViewName() {
    return this.stack.at(-1)!.name;
  }

  public clear() {
    this.stack = [];
  }

  public async rerender(): Promise<void> {
    await this.push(this.stack.pop()!);
  }

  public async push(view: View<C>): Promise<void> {
    // Add the view to the stack
    this.stack.push(view);
    const nextView = await view.render(this);
    // Clear the container
    this.container.innerHTML = "";
    // Render the new view
    try {
      this.container.appendChild(nextView);
    } catch (e) {
      console.error(e);
    }
  }

  public async pop(): Promise<void> {
    // If there are views in the stack
    if (this.stack.length > 0) {
      // Remove the top view
      this.stack.pop();
      // If there are still views in the stack
      if (this.stack.length > 0) {
        const nextView = await this.stack[this.stack.length - 1]!.render(this); // Clear the container
        this.container.innerHTML = "";
        // Render the top view
        this.container.appendChild(nextView);
      }
    }
  }

  public async popToRoot(): Promise<void> {
    // While there are views in the stack
    while (this.stack.length > 1) {
      // Remove the top view
      this.stack.pop();
    }
    const nextView = await this.stack[0]!.render(this);
    // Clear the container
    this.container.innerHTML = "";
    // Render the top view
    this.container.appendChild(nextView);
  }
}
